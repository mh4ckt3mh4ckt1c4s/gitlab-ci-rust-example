pub mod tests;

use gitlab_ci_example::is_prime;
use std::io;

fn main() {
    println!("Enter a number to check if it's a prime:");

    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let number: u64 = input.trim().parse().expect("Please type a number!");

    let prime_status = is_prime(number);

    if prime_status {
        println!("{} is a prime number.", number);
    } else {
        println!("{} is not a prime number.", number);
    }
}
