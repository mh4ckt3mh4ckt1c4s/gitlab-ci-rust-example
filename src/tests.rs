#[cfg(test)]
use super::*;

#[test]
fn test_is_prime_with_prime_number() {
    assert!(is_prime(2));
    assert!(is_prime(3));
    assert!(is_prime(5));
    assert!(is_prime(7));
    assert!(is_prime(11));
}

#[test]
fn test_is_prime_with_non_prime_number() {
    assert!(!is_prime(4));
    assert!(!is_prime(6));
    assert!(!is_prime(8));
    assert!(!is_prime(9));
    assert!(!is_prime(12));
}

#[test]
fn test_is_prime_with_zero() {
    assert!(!is_prime(0));
}

#[test]
fn test_is_prime_with_one() {
    assert!(!is_prime(1));
}

#[test]
fn test_is_prime_with_large_prime() {
    assert!(is_prime(7919));
}

#[test]
fn test_is_prime_with_large_non_prime() {
    assert!(!is_prime(7920));
}
