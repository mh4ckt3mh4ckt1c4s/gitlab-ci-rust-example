#![no_main]

extern crate gitlab_ci_example;

use libfuzzer_sys::fuzz_target;
use gitlab_ci_example::is_prime;

// fuzz with the u8 type to test only small numbers
fuzz_target!(|data: u8| {
    let _ = is_prime(data as u64);
});