# Example of a GitLab CI with Rust

This repository holds the example project I used to teach the [CI security course](https://gitlab.com/mh4ckt3mh4ckt1c4s/ci-security-course) I gave at Télécom SudParis.

## Using this repository

On the `main` branch is the project with all CI stages broken. The goal is to understand for each job in the CI what the job is doing, find out why it is failing, and fix the Rust project accordingly. You can find a correction with the fixed project and a passing CI on the `correction` branch, with one commit for each job fix.

The `correction` branch is completely correct only once merged into the main branch (because of the `container_scanning` job that scans only containers tagged with `latest`, i.e. containers built from the main branch).

## License

This project is under the MIT license. See more in the [LICENSE](LICENSE).
